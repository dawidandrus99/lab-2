const GAME_WIDTH = 800;
const GAME_HEIGHT = 600;

const clamp = (num, min, max) => Math.min(Math.max(num, min), max);

class Pew {
    constructor(source) {
        this.canvas = document.createElement('CANVAS');
        this.canvas.setAttribute("width", 2);
        this.canvas.setAttribute("height", 10);
        this.x = source.originX;
        this.y = source.originY;
        this.m = source.movement;

        const ctx = this.canvas.getContext('2d');
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, 2, 10);

        this.move();
    }

    move() {
        this.y += this.m;
        setTimeout(() => this.move(), 100);
    }

    draw(ctx) {
        ctx.drawImage(this.canvas, this.x, this.y);
    }
}

class Invader {
    constructor(type, x, y) {
        this.x = x;
        this.y = y;
        this.imageIndex = 0;
        this.image = [new Image(), new Image()];
        this.image[0].src = `ass/Invader${type}1.png`;
        this.image[1].src = `ass/Invader${type}2.png`;
        this.m = 4;
        this.stepCount = 0;
        this.swap();
        this.pew = null;
    }

    fire() {
        if (!this.pew) {
            this.pew = new Pew(this);
        }
    }

    contains(fire) {
        if (fire) {
            const width = this.image[0].width;
            const height = this.image[0].height;
            return fire.x >= this.x &&
                fire.x + 1 <= this.x + width &&
                fire.y < this.y + height;
        }
        return false;
    }

    swap() {
        this.imageIndex = ++this.imageIndex % 2;
        this.stepCount++;
        if (this.stepCount >= 24) {
            this.stepCount = 0;
            this.m = -this.m;
            this.y += 10;
        } else {
            this.x += this.m;
        }
        setTimeout(() => this.swap(), 1000);
    }

    get originX() {
        return this.x + this.image[0].width / 2;
    }

    get originY() {
        return this.y + this.image[0].height + 2;
    }

    get movement() {
        return 8;
    }

    draw(ctx) {
        if (this.image[this.imageIndex].complete) {
            ctx.drawImage(this.image[this.imageIndex], this.x, this.y);
        }
        if (this.pew) {
            if (this.pew.y < 0) {
                this.pew = null;
                return;
            }
            this.pew.draw(ctx);
        }
    }
}

class Ship {
    constructor() {
        this.loaded = false;
        this.image = new Image();
        this.x = 0;
        this.image.addEventListener('load', () => {
            this.loaded = true;
            this.x = GAME_WIDTH / 2 - this.image.width / 2;
        });
        this.image.src = 'ass/Ship.png';
        this.isMoving = false;
        this.speed = 0;
        this.pew = null;
    }

    fire() {
        if (!this.pew) {
            this.pew = new Pew(this);
        }
    }

    contains(fire) {
        if (fire) {
            return fire.x >= this.x &&
                fire.x + 1 <= this.x + this.image.width &&
                fire.y > GAME_HEIGHT - 50;
        }
        return false;
    }

    moveLeft() {
        this.speed = -16;
        if (!this.isMoving) {
            this.isMoving = true;
        }
        this.move();
    }

    moveRight() {
        this.speed = 16;
        if (!this.isMoving) {
            this.isMoving = true;
        }
        this.move();
    }

    move() {
        this.position = this.x + this.speed;
        if (this.isMoving) {
            setTimeout(() => this.move(), 100);
        }
    }

    stop() {
        this.isMoving = false;
    }

    set position(value) {
        const maxWidth = GAME_WIDTH - this.image.width;
        this.x = clamp(value, 0, maxWidth);
    }

    get originX() {
        return this.x + this.image.width / 2;
    }

    get originY() {
        return 500;
    }

    get movement() {
        return -16;
    }

    draw(ctx) {
        ctx.drawImage(this.image, this.x, GAME_HEIGHT - 50);
        if (this.pew) {
            if (this.pew.y < 0) {
                this.pew = null;
                return;
            }
            this.pew.draw(ctx);
        }
    }
}

class SpaceInvaders {
    constructor() {
        this.ship = new Ship();
        this.invaders = [];
        this.fire = [];
        this.canvas = document.createElement('CANVAS');
        this.canvas.setAttribute("width", GAME_WIDTH);
        this.canvas.setAttribute("height", GAME_HEIGHT);
        // this.canvas.addEventListener("mousemove", e => {
        //     this.ship.position = event.offsetX;
        //     this.draw();
        // });
        this.ctx = this.canvas.getContext('2d');

        for (let i = 0; i < 8; ++i) {
            this.invaders.push(new Invader('C', 80 + i * 60, 80));
            this.invaders.push(new Invader('B', 80 + i * 60, 130));
            this.invaders.push(new Invader('B', 80 + i * 60, 180));
            this.invaders.push(new Invader('A', 80 + i * 60, 230));
            this.invaders.push(new Invader('A', 80 + i * 60, 280));
        }
        setInterval(() => this.invaderFire(), 1000);
    }

    invaderFire() {
        if (this.invaders.length === 0) {
            return;
        }
        const invaderIndex = Math.floor(Math.random() * this.invaders.length);
        this.invaders[invaderIndex].fire();

    }

    draw() {
        this.ctx.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        this.ship.draw(this.ctx);

        for (let i = this.invaders.length - 1; i >= 0; --i) {
            //for (let f = this.fire.length - 1; f >= 0; --f) {
            if (this.ship.contains(this.invaders[i].pew)) {
                this.invaders[i].pew = null;
                console.warn("Game Over");
            }
            if (this.invaders[i].contains(this.ship.pew)) {
                // this.fire.splice(f, 1);
                this.ship.pew = null;
                this.invaders.splice(i, 1);
                break;
            }
            // }
        }

        for (let f = this.fire.length - 1; f > 0; --f) {
            if (this.fire[f].y < 0 || this.fire[f].y > GAME_HEIGHT) {
                this.fire.splice(f, 1);
            }
        }

        this.invaders.forEach(invader => !invader.deleted && invader.draw(this.ctx));
        //this.fire.forEach(fire => !fire.deleted && fire.draw(this.ctx));
        window.requestAnimationFrame(() => this.draw());
    }

    handleKey(event) {
        console.log(event.code);
        if (event.type === 'keydown') {
            switch (event.code) {
                case 'Space':
                    this.ship.fire();
                    break;
                case 'ArrowLeft':
                    this.ship.moveLeft();
                    break;
                case 'ArrowRight':
                    this.ship.moveRight();
                    break;
            }
        } else if (event.code.includes('Arrow')) {
            this.ship.stop();
        }
    }

    init() {
        document.body.appendChild(this.canvas);
        this.draw();
    }
}

const game = new SpaceInvaders();

window.addEventListener('keydown', event => !event.repeat && game.handleKey(event));
window.addEventListener('keyup', event => !event.repeat && game.handleKey(event));
window.addEventListener('DOMContentLoaded', () => game.init());