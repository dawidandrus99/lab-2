class Calc {
    constructor() {
        this.value = 0;
        this.op = 0;

        this.container = document.createElement('DIV');
        this.container.id = 'container';

        this.screen = document.createElement('DIV');
        this.screen.id = 'screen';
        this.screen.textContent = 0;
        this.container.appendChild(this.screen);

        [7,8,9,'C',4,5,6,'+',1,2,3,'-',0,'.','='].forEach(item => {
            const button = document.createElement('DIV');
            button.textContent = item;
            button.addEventListener('click', event => this.mouseClick(event));
            if (item === 0) button.className = 'wide';
            this.container.appendChild(button);
        });
    }

    get screenValue(){
        return parseFloat(this.screen.textContent);
    }

    set screenValue(value){
        this.screen.textContent = value;
    }

    changebuffer() {
        switch (this.op) {
            case -1:
                this.value -= this.screenValue;
                break;
            case 0:
                this.value = this.screenValue;
                break;
            case 1:
                this.value += this.screenValue;
                break;
        }
    }

    mouseClick(event) {
        const key = event.target.textContent;
        switch (key) {
            case 'C':
                this.value = 0;
                this.op = 0;
                this.screenValue = 0;
                break;
            case '+':
                this.changebuffer();
                this.op = 1;
                this.screenValue = 0;
                break;
            case '-':
                this.changebuffer();
                this.op = -1;
                this.screenValue = 0;
                break;
            case '=':
                this.changebuffer();
                this.op = 0;
                this.screenValue = this.value;
                break;
            case '.':
                if (!this.screen.textContent.includes('.')) {
                    this.screen.textContent += key;
                }
                break;
            default:
                if (this.screen.textContent.length > 8) {
                    return;
                }
                if (this.screen.textContent === '0') {
                    this.screenValue = key;
                }
                else {
                    this.screen.textContent += key;
                }
                break;
        }
    }

    init() {
      document.body.appendChild(this.container);
    }
}

const calc = new Calc();

window.addEventListener('DOMContentLoaded', () => calc.init());